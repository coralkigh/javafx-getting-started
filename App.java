package org.openjfx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"));
        scene.getStylesheets().add(App.class.getResource("styles.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
        Thread t = new Thread(){
            public void run(){
                try{
                    Game game = new Game();
                    ServerSocket ss = new ServerSocket(6666);
                    while(true){
                        Socket s = ss.accept();
                        DataInputStream dis = new DataInputStream(s.getInputStream());
                        String str =(String)dis.readUTF();

                        game.won();
                        game.isnull();
                        if(game.gagne ==0 ){

                            System.out.println("Tie");break;
                        }
                        if(game.gagne == -1 ){

                            System.out.println("Lost");break;
                        }
                        if(game.gagne ==1 ){

                            System.out.println("Win");break;
                        }
                        // Button modification
                        game.won();
                        game.isnull();
                        if(game.gagne ==0 ){

                            System.out.println("Tie");break;
                        }
                        if(game.gagne == -1 ){

                            System.out.println("Lost");break;
                        }
                        if(game.gagne ==1 ){

                            System.out.println("Win");break;
                        }
                        game.joueur = -(game.joueur);


                    }
                    System.out.println("Thanks for playing !");
                } catch(Exception e){
                    System.err.println(e);
                }
            }
        };
        t.start();
    }

}