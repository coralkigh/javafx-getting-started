package org.openjfx;

import javafx.application.Platform;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class MyServer extends Thread {
    private PrimaryController primaryController;

    public MyServer(PrimaryController primaryController) {
        this.primaryController = primaryController;
    }

    @Override
    public void run() {
        try {
            System.out.println("iniciando");
            ServerSocket ss = new ServerSocket(6665);

            while(true) {
                Socket s = ss.accept();
                DataInputStream dis = new DataInputStream(s.getInputStream());

                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                final String palabra = (String) dis.readUTF();
                System.out.println("message= " + palabra);
                Thread updater = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        primaryController.updateUI(Integer.valueOf(palabra));
                    }
                });
                Platform.runLater(updater);
                if(Integer.valueOf(palabra)==9){    //THE Button 9 doesnt exist, so its the keyword for closing
                    break;
                };

            }
            ss.close();
        } catch (Exception e) {
            System.err.println(e);
        };
    }
}
