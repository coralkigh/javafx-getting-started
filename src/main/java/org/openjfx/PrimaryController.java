package org.openjfx;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class PrimaryController {

      Button button_0;
      Button button_1;
      Button button_2;
      Button button_3;
      Button button_4;
      Button button_5;
      Button button_6;
      Button button_7;
      Button button_8;

      @FXML
      GridPane myGridpane = new GridPane();

      public void SetGridPane() {
            button_0 = new Button();
            button_0.setPrefHeight(100);
            button_0.setPrefWidth(100);
            GridPane.setConstraints(button_0, 0, 0);

            button_1 = new Button();
            button_1.setPrefHeight(100);
            button_1.setPrefWidth(100);
            GridPane.setConstraints(button_1, 1, 0);

            button_2 = new Button();
            button_2.setPrefHeight(100);
            button_2.setPrefWidth(100);
            GridPane.setConstraints(button_2, 2, 0);

            button_3 = new Button();
            button_3.setPrefHeight(100);
            button_3.setPrefWidth(100);
            GridPane.setConstraints(button_3, 0, 1);

            button_4 = new Button();
            button_4.setPrefHeight(100);
            button_4.setPrefWidth(100);
            button_4.setAlignment(Pos.CENTER);
            GridPane.setConstraints(button_4, 1, 1);


            button_5 = new Button();
            button_5.setPrefHeight(100);
            button_5.setPrefWidth(100);
            GridPane.setConstraints(button_5, 2, 1);

            button_6 = new Button();
            button_6.setPrefHeight(100);
            button_6.setPrefWidth(100);
            GridPane.setConstraints(button_6, 0, 2);

            button_7 = new Button();
            button_7.setPrefHeight(100);
            button_7.setPrefWidth(100);
            GridPane.setConstraints(button_7, 1, 2);

            button_8 = new Button();
            button_8.setPrefHeight(100);
            button_8.setPrefWidth(100);
            GridPane.setConstraints(button_8, 2, 2);

            // don't forget to add children to gridpane
            myGridpane.getChildren().addAll(button_0, button_1, button_2, button_3, button_4, button_5, button_6,
                    button_7,button_8);

      }

      @FXML
      public void initialize(){
            SetGridPane();
            MyServer server = new MyServer(this);
            server.setDaemon(true);
            server.start();
            String ip = "localhost";

            button_0.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 0");
                                                      button_0.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("0");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });

            button_1.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 1");
                                                      button_1.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("1");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });

            button_2.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 2");
                                                      button_2.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("2");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });

            button_3.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 3");
                                                      button_1.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("3");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });

            button_4.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 3");
                                                      button_4.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("4");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });

            button_5.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 5");
                                                      button_5.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("5");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });

            button_6.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 6");
                                                      button_1.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("6");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });

            button_7.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 7");
                                                      button_1.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("7");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });
            
            button_8.setOnAction(new EventHandler<ActionEvent>() {
                  @Override
                  public void handle(ActionEvent actionEvent) {
                        Thread t = new Thread(new Runnable() {
                              @Override
                              public void run() {
                                    Runnable updater = new Runnable() {
                                          @Override
                                          public void run() {
                                                try {
                                                      System.out.println("Boton 8");
                                                      button_1.setText("X");
                                                      Socket s = new Socket(ip, 6665);
                                                      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
                                                      dout.writeUTF("8");
                                                      dout.flush();
                                                      dout.close();
                                                      s.close();
                                                } catch(Exception e){
                                                      System.err.println(e);
                                                }
                                          }
                                    };
                                    Platform.runLater(updater);
                              }
                        });
                        t.setDaemon(true);
                        t.start();
                  }
            });

      }

      public void updateUI(Integer index){
            switch (index){
                  case 0:
                        button_0.setText("O");
                        break;
                  case 1:
                        button_1.setText("O");
                        break;
                  case 2:
                        button_2.setText("O");
                        break;
                  case 3:
                        button_3.setText("O");
                        break;
                  case 4:
                        button_4.setText("O");
                        break;
                  case 5:
                        button_5.setText("O");
                        break;
                  case 6:
                        button_6.setText("O");
                        break;
                  case 7:
                        button_7.setText("O");
                        break;
                  case 8:
                        button_8.setText("O");
                        break;
            }
      }
}
